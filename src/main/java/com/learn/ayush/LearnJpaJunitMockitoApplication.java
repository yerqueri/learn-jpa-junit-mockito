package com.learn.ayush;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LearnJpaJunitMockitoApplication {

	public static void main(String[] args) {
		SpringApplication.run(LearnJpaJunitMockitoApplication.class, args);
	}
}
